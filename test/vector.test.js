import test from 'tape'

import Vector from '../public/lib/vector.js'

test('Vector', t => {
  const a = new Vector(3, 4)

  t.equal(a.x, 3, 'x')
  t.equal(a.y, 4, 'y')
  t.deepEqual(a.coords, [3, 4], 'coords')
  t.deepEqual(a.length, 5, 'length')

  /* mutations */
  let result

  const b = new Vector(1, 2)

  result = a.add(b)
  t.deepEqual(
    [result, a.x, a.y],
    [a, 4, 6],
    'add returns original vector, mutated'
  )

  result = a.subtract(b)
  t.deepEqual(
    [result, a.x, a.y],
    [a, 3, 4],
    'subtract returns original vector, mutated'
  )

  result = a.copy()
  t.notEqual(result, a, 'copy makes a new vector')
  t.deepEqual(result.coords, a.coords, 'copy has same coords')

  result = a.scale(2)
  t.deepEqual(
    [result, a.x, a.y],
    [a, 6, 8],
    'scale returns original vector, mutated'
  )

  result = a.scaleTo(2)
  t.deepEqual(
    [result, round(a.x, 1), a.y],
    [a, round(2 * 3 / 5, 1), 2 * 4 / 5],
    'scale returns original vector, mutated'
  )

  t.end()
})

function round (num, decimalPlaces = 0) {
  const k = Math.pow(10, decimalPlaces)
  return Math.round(num * k) / k
}
