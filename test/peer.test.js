import test from 'tape'

import Peer from '../public/lib/peer.js'
import Vector from '../public/lib/vector.js'
import pull from '../public/lib/pull.js'

test('new Peer', t => {
  const a = new Peer('A')

  t.equal(a.id, 'A', 'id')
  t.equal(a.coords.constructor, Vector, 'has coords (Vector)')
  t.equal(a.log.constructor, Array, 'has log (Array)')

  t.end()
})

test('peer.publish', t => {
  const a = new Peer('A')

  a.publish()
  a.publish({ type: 'contact', contact: 'B' })

  t.deepEqual(
    a.log,
    [
      { author: 'A', seq: 1 },
      { author: 'A', seq: 2, content: { type: 'contact', contact: 'B' } }
    ],
    'publish adds to the log'
  )
  t.equal(a.getLatestSequence('A'), 2, 'updates getLatestSequence')

  t.end()
})

test('peer.add', t => {
  const a = new Peer('A')

  a.add({ author: 'B', seq: 1 })
  a.add({ author: 'B', seq: 2, content: { type: 'boop' } })
  t.deepEqual(
    a.log,
    [
      { author: 'B', seq: 1 },
      { author: 'B', seq: 2, content: { type: 'boop' } }
    ],
    'add adds to the log'
  )

  t.equal(a.getLatestSequence('B'), 2, 'updates getLatestSequence')

  t.throws(
    () => a.add({ author: 'B', seq: 20 }),
    /wrong seq/,
    'does not allow adding wrong seq'
  )

  t.end()
})

test('peer.createHistoryStream', t => {
  t.plan(2)
  const a = new Peer('A')

  a.publish()
  a.add({ author: 'B', seq: 1 })
  a.publish({ type: 'contact', contact: 'B' })

  pull(
    a.createHistoryStream({ id: 'A' }),
    pull.collect((_, results) => {
      t.deepEqual(
        results,
        [
          { author: 'A', seq: 1 },
          { author: 'A', seq: 2, content: { type: 'contact', contact: 'B' } }
        ],
        '{ id }'
      )
    })
  )

  pull(
    a.createHistoryStream({ id: 'A', since: 1 }),
    pull.collect((_, results) => {
      t.deepEqual(
        results,
        [
          { author: 'A', seq: 2, content: { type: 'contact', contact: 'B' } }
        ],
        '{ id, since }'
      )
    })
  )
})

test('peer.peersToReplicate', t => {
  const a = new Peer('A')

  const follow = (id) => ({ type: 'contact', contact: id })

  a.publish(follow('B'))
  a.add({ author: 'B', seq: 1, content: follow('A') })
  a.add({ author: 'B', seq: 2, content: follow('C') })
  a.add({ author: 'C', seq: 1, content: follow('X') })

  t.deepEqual(
    a.peersToReplicate().sort(),
    ['B', 'C'].sort(),
    'peersToReplicate'
  )

  t.end()
})
