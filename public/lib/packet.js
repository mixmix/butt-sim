/* eslint brace-style: ["error", "stroustrup", { "allowSingleLine": true }] */

const DISTANCE_PER_TICK = 3

/* NOTES
  - looks weird when peers move... I think because I haven't integrated
    velocity / acceleration, so movement doesn't feel "physical"
  - if the "from Peer" moves towards "to Peer" faster than DISTANCE_PER_TICK,
    then packets will arrive out of sequence, and replication will (this time)
*/

export default class Packet {
  constructor ({ from, to, msg }) {
    this.from = from // the peer it cam from
    this.to = to // the peer it's going to
    this.msg = msg // the msg being transferred
    this.pos = from.pos.copy() // the packets position
  }

  tick () {
    if (this.pos === null) return

    // calculate vector packet --> to
    const vector = this.to.pos.copy()
      .subtract(this.pos)

    const d = vector.length
    if (d <= 10) {
      // complete replication to "to peer"
      this.to.add(this.msg)
      // remove the node packet from space
      this.pos = null
      return
    }

    this.pos.add(
      vector.scale(DISTANCE_PER_TICK / d)
      // could use vector.scaleTo, but this saves calculating vector.length again
    )
  }

  isEqual (packet) {
    return (
      this.from.id === packet.from.id &&
      this.to.id === packet.to.id &&
      this.msg.author === packet.msg.author &&
      this.msg.seq === packet.msg.seq
    )
  }
}
