export default class Vector {
  constructor (x, y) {
    if (
      typeof x !== 'number' ||
      typeof y !== 'number'
    ) throw new Error('gimme numbers please')

    this.coords = [x, y]
  }

  /* getters */
  get x () { return this.coords[0] }
  get y () { return this.coords[1] }

  get length () {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
  }

  get unit () {
    const len = this.length
    return [this.x / len, this.y / len]
  }

  /* mutating methods */
  add (vector) {
    this.coords[0] = this.x + vector.x
    this.coords[1] = this.y + vector.y
    return this
  }

  subtract (vector) {
    this.coords[0] = this.x - vector.x
    this.coords[1] = this.y - vector.y
    return this
  }

  scale (k) {
    this.coords[0] = k * this.x
    this.coords[1] = k * this.y
    return this
  }

  scaleTo (k) {
    return this.scale(k / this.length)
  }

  copy () {
    return new Vector(this.x, this.y)
  }
}
