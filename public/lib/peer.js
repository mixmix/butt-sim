/* eslint brace-style: ["error", "stroustrup", { "allowSingleLine": true }] */

import Vector from './vector.js'
import pull from './pull.js'

export default class Peer {
  constructor (id, opts = {}) {
    if (!id) throw new Error('needs an id')

    this.id = id
    this.log = []
    this.index = {
      seq: new SeqIndex(), //       map peerId --> latestSeq
      follow: new FollowIndex(), // map peerIdA --follow--> peerIdB
      author: new Set() //          peerIds in log
    }

    this.color = opts.color
    this.pos = opts.pos || new Vector(0, 0)
  }

  publish (content) {
    const msg = {
      author: this.id,
      seq: null // added in #add
    }
    if (typeof content === 'object') msg.content = content
    // does not allow encrypted content atm

    this.add(msg)
  }

  add (msg) {
    // if the author is me and I forgot the msg.seq, attach one
    if (msg.author === this.id && msg.seq === null) {
      msg.seq = this.getLatestSequence(msg.author) + 1
    }
    // otherwise, check this message is correct
    else if (msg.seq !== this.getLatestSequence(msg.author) + 1) {
      console.log('wrong seq', {
        peer: this.id,
        msg,
        latestSeq: this.getLatestSequence(msg.author)
      })
      // these can happen if pulling updates from multiple peers
      return
    }

    this.log.push(msg)

    /* update indexs */
    this.index.seq.process(msg)
    this.index.follow.process(msg)
    this.index.author.add(msg.author)
  }

  getLatestSequence (id) {
    return this.index.seq.get(id)
  }

  createHistoryStream ({ id, since, timeout = 20 } = {}) {
    if (!id) throw new Error('id required')

    return pull(
      pull.values(this.log),
      pull.filter(msg => msg.author === id),
      since
        ? pull.filter(msg => msg.seq > since)
        : null,

      timeout
        ? pull.asyncMap((msg, cb) => {
            setTimeout(() => cb(null, msg), timeout)
          })
        : null
    )
  }

  peersToReplicate () {
    /* 1 hop */
    const ids = new Set(this.index.follow.get(this.id))

    /* 2 hops */
    this.index.follow.get(this.id).forEach(id => {
      this.index.follow.get(id).forEach(id => {
        ids.add(id)
      })
    })

    ids.delete(this.id)

    return [...ids]
  }
}

class SeqIndex {
  constructor () {
    this.index = {}
  }

  process (msg) {
    this.increment(msg.author)
  }

  increment (author) {
    if (!this.index[author]) this.index[author] = 0
    this.index[author]++
  }

  get (author) {
    return this.index[author] || 0
  }
}

class FollowIndex {
  constructor () {
    this.index = {}
  }

  process (msg) {
    if (!isFollow(msg)) return
    this.add(msg.author, msg.content.contact)
  }

  add (author, target) {
    // author follows target
    if (!this.index[author]) this.index[author] = new Set()
    this.index[author].add(target)
  }

  get (author) {
    return this.index[author] || new Set()
  }
}
function isFollow (msg) {
  return (
    msg.content &&
    msg.content.type === 'contact' &&
    typeof msg.content.contact === 'string'
  )
}
