/* eslint brace-style: ["error", "stroustrup", { "allowSingleLine": true }] */
import Peer from './lib/peer.js'
import Packet from './lib/packet.js'
import Vector from './lib/vector.js'
import pull from './lib/pull.js'

const canvas = setupCanvas(document.getElementById('app'))

/* setup - canvas + consts */
const PEER_RADIUS = 30

const width = canvas.clientWidth
const height = canvas.clientHeight
const center = { x: width / 2, y: height / 2 }
const offset = (height / 2 < 400) ? height / 2 - 50 : 400

const n = 5
const angle = 2 * Math.PI / n
const coords = new Array(n).fill(offset)
  .map((k, i) => [
    k * Math.sin(i * angle) + center.x,
    k * Math.cos(i * angle) + center.y - 20
  ])
const colors = [
  '#ef476f',
  '#ffd166',
  '#06d6a0',
  '#118ab2',
  '#073b4c'
].sort((a, b) => Math.random() - 0.5)

/* setup (state) */
const A = new Peer('A', { color: colors[0], pos: new Vector(...coords[0]) })
const B = new Peer('B', { color: colors[1], pos: new Vector(...coords[1]) })
const C = new Peer('C', { color: colors[2], pos: new Vector(...coords[2]) })
const D = new Peer('D', { color: colors[3], pos: new Vector(...coords[3]) })
const E = new Peer('E', { color: colors[4], pos: new Vector(...coords[4]) })

const pubA = new Peer('pubA', { color: 'grey', pos: new Vector(center.x + 70, center.y + 70) })
const pubB = new Peer('pubB', { color: 'lightgrey', pos: new Vector(center.x - 70, center.y - 50) })

const state = {
  peers: { A, B, C, D, E, pubA, pubB },
  packets: [],
  hoveredPeerId: null,
  selectedPeerId: null,
  isMouseDown: false
}

/* setup (follows) */
// use some pub invites
joinPub(pubA, A)
joinPub(pubA, B)
joinPub(pubA, C)
joinPub(pubA, D)
joinPub(pubB, D)
joinPub(pubB, E)
function joinPub (pub, peer) {
  peer.publish({ type: 'contact', contact: pub.id })
  pub.publish({ type: 'contact', contact: peer.id })
}

// peers follow each other
A.publish({ type: 'contact', contact: 'C' }) // A follows C
B.publish({ type: 'contact', contact: 'A' }) // B follows A
C.publish({ type: 'contact', contact: 'B' }) // C follows A

/* setup (peers publishing) */
// IDEA: add a hook to peer.add which has some chance of replying to the message!
//       % chance could be dependant on past replies and current follows!
publishPeriodically(A, 5)
publishPeriodically(B, 10)
publishPeriodically(C, 4)
publishPeriodically(D, 7)
publishPeriodically(E, 10)
function publishPeriodically (peer, period) {
  pull(
    pull.infinite(),
    pull.asyncMap((_, cb) => setTimeout(cb, 500 + period * 1000 * Math.random())),
    pull.asyncMap((_, cb) => {
      // console.log(peer.id, 'says something')
      peer.publish()
      cb(null)
    }),
    pull.drain()
  )
}

/* setup (connections) */
// TODO: add idea of "pubs" / static IPs
connectPeriodically(A, pubA, 1)
connectPeriodically(A, pubB, 1)
connectPeriodically(B, pubA, 6)
connectPeriodically(B, pubB, 10)
connectPeriodically(C, pubA, 7)
connectPeriodically(C, pubB, 10)
connectPeriodically(D, pubA, 10)
connectPeriodically(D, pubB, 7)
connectPeriodically(E, pubA, 15)
connectPeriodically(E, pubB, 10)
connectPeriodically(pubA, pubB, 3)
function connectPeriodically (A, B, period) {
  pull(
    pull.infinite(),
    pull.asyncMap((_, cb) => setTimeout(cb, period * 1000 * Math.random())),
    pull.asyncMap((_, cb) => connect(A, B, cb)),
    pull.drain()
  )
}

function connect (A, B, cb) {
  // A + B exchange desired updates till "done"

  // not really sure what a "connection" will look like in this app,
  // but this will do for now

  let running = 2
  const done = () => {
    running--
    if (running === 0) {
      cb(null)
    }
  }

  // each peer asks the other for updates
  pullUpdates({ from: A, to: B }, done)
  pullUpdates({ from: B, to: A }, done)
}

function pullUpdates ({ from: fromPeer, to: toPeer }, cb) {
  pull(
    pull.values(toPeer.peersToReplicate()),
    pull.asyncMap((peerId, cb) => {
      pull(
        fromPeer.createHistoryStream({
          id: peerId,
          since: toPeer.getLatestSequence(peerId),
          timeout: 80
        }),
        pull.drain(
          msg => {
            const newPacket = new Packet({
              from: fromPeer,
              to: toPeer,
              msg
            })
            // drop if there's already a packet like this
            const isExistingPacket = state.packets.find(packet => packet.isEqual(newPacket))
            if (isExistingPacket) {
              return
            }

            state.packets.push(newPacket)
          },
          cb
        )
      )
    }),
    pull.collect(cb)
  )
}

// TODO:
// - set up follows!
//   - publish follow messages!
// - set up replicator
//    - connections
//    - loops through connections * peersToReplicate * createHistoryStream
// - set up periodic publishing
// - add render methods
//    - peer
//      - log?
//      - how does peer know color of msgs... id is a color?
//    - packet

/* make peers moveable */
canvas.addEventListener('mousemove', e => {
  if (state.selectedPeerId) {
    state.peers[state.selectedPeerId].pos.coords = [e.offsetX, e.offsetY]
  }
  else {
    canvas.setAttribute(
      'style',
      findPeerId(e.offsetX, e.offsetY) ? 'cursor: grab' : ''
    )
  }
})
canvas.addEventListener('mousedown', e => {
  state.selectedPeerId = findPeerId(e.offsetX, e.offsetY)
  if (state.selectedPeerId) {
    canvas.setAttribute('style', 'cursor: grabbing')
  }
})
canvas.addEventListener('mouseup', e => {
  state.selectedPeerId = null
  canvas.setAttribute('style', '')
})
function findPeerId (x, y) {
  return Object.keys(state.peers)
    .find(peerId => {
      const peer = state.peers[peerId]
      return (
        x >= peer.pos.x - PEER_RADIUS &&
        x <= peer.pos.x + PEER_RADIUS &&
        y >= peer.pos.y - PEER_RADIUS &&
        y <= peer.pos.y + PEER_RADIUS
      )
    })
}

// //

const tick = buildTick(canvas, state)

startAnimatLoop(tick)

function startAnimatLoop (tick) {
  const FRAME_RATE = 5
  const PERIOD = 1 / FRAME_RATE

  let lastTime = 0
  animate()

  function animate (timestamp) {
    if (timestamp >= lastTime + PERIOD) {
      lastTime = timestamp
      tick()
    }

    requestAnimationFrame(animate) // eslint-disable-line
  }
}

function setupCanvas (el) {
  const canvas = document.createElement('canvas')
  canvas.setAttribute('height', el.clientHeight)
  canvas.setAttribute('width', el.clientWidth)

  el.appendChild(canvas)
  return canvas
}

function buildTick (canvas, state) {
  const ctx = canvas.getContext('2d')
  ctx.strokeStyle = 'none'
  const PACKET_RADIUS = 2
  const LOG_MSG_SIZE = 2 * PACKET_RADIUS
  const LOG_HALF_WIDTH = PEER_RADIUS - 2 * LOG_MSG_SIZE
  const MAX_PER_ROW = 2 * LOG_HALF_WIDTH / LOG_MSG_SIZE

  return function tick () {
    /* Redraw */
    // - clear
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    // - draw active connections
    state.packets.forEach(packet => {
      // TODO this is *pretty wasteful* atm
      drawConnection(packet.from, packet.to)
    })

    // - draw packets
    state.packets.forEach(drawPacket)

    // - draw peers
    Object.values(state.peers).forEach(drawPeer)
    // - draw peer logs
    Object.values(state.peers).forEach(drawLog)

    /* Mutate state */
    // - move packets
    state.packets.forEach(packet => packet.tick())

    // - drop the packets that don't have a position anymore
    state.packets = state.packets.filter(packet => packet.pos !== null)
  }

  function drawPeer (peer) {
    ctx.beginPath()
    ctx.arc(peer.pos.x, peer.pos.y, PEER_RADIUS, 0, 2 * Math.PI)

    ctx.fillStyle = peer.color
    ctx.fill()
    // ctx.strokeStyle = peer.color
    // ctx.stroke()
  }

  function drawLog (peer) {
    const x = peer.pos.x - LOG_HALF_WIDTH
    let y = peer.pos.y + PEER_RADIUS + 10

    let fullRows, remainder
    peer.index.author.forEach(peerId => {
      // const initialY = y
      const count = peer.log.reduce((acc, msg) => msg.author === peerId ? acc + 1 : acc, 0)
      // const actualCount = state.peers[peerId].getLatestSequence(peerId)

      /* draw actual state */
      // let fullRows = Math.floor(actualCount / MAX_PER_ROW)
      // let remainder = actualCount % MAX_PER_ROW
      // ctx.fillStyle = 'black'

      // // draw full rows + move down
      // if (fullRows) {
      //   ctx.fillRect(x, y, 2 * LOG_HALF_WIDTH, fullRows * LOG_MSG_SIZE)
      //   y += fullRows * LOG_MSG_SIZE
      // }
      // // draw remainder row + move down
      // if (remainder) {
      //   ctx.fillRect(x, y, remainder * LOG_MSG_SIZE, LOG_MSG_SIZE)
      //   y += LOG_MSG_SIZE
      // }
      // const maxY = y

      /* draw local state */
      // y = initialY
      fullRows = Math.floor(count / MAX_PER_ROW)
      remainder = count % MAX_PER_ROW
      ctx.fillStyle = state.peers[peerId].color

      // draw full rows + move down
      if (fullRows) {
        ctx.fillRect(x, y, 2 * LOG_HALF_WIDTH, fullRows * LOG_MSG_SIZE)
        y += fullRows * LOG_MSG_SIZE
      }
      // draw remainder row + move down
      if (remainder) {
        ctx.fillRect(x, y, remainder * LOG_MSG_SIZE, LOG_MSG_SIZE)
        y += LOG_MSG_SIZE
      }

      // gap before next peer
      // y = maxY + 1
      y += 1
    })
  }

  function drawConnection (A, B) {
    ctx.beginPath()
    ctx.moveTo(A.pos.x, A.pos.y)
    ctx.lineTo(B.pos.x, B.pos.y)
    ctx.strokeStyle = '#eee'
    ctx.lineWidth = 8
    ctx.stroke()
  }

  function drawPacket (packet) {
    ctx.fillStyle = state.peers[packet.msg.author].color
    ctx.fillRect(packet.pos.x - PACKET_RADIUS, packet.pos.y - PACKET_RADIUS, 2 * PACKET_RADIUS, 2 * PACKET_RADIUS)
  }
}
